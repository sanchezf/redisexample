using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using RedisExample.Api.Models;
using StackExchange.Redis;

namespace RedisExample.Api.Data
{
    public class RedisPlatformRepository : IPlatformRepository
    {
        private const string PlatformsHashName = "PlatformsHash";
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        public RedisPlatformRepository(IConnectionMultiplexer connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer;
        }
        
        public void CreatePlatform(Platform platform)
        {
            if (platform == null) throw new ArgumentNullException(nameof(platform));
            var db = _connectionMultiplexer.GetDatabase();
            var serializedPlat = JsonSerializer.Serialize(platform);
            db.HashSet(PlatformsHashName, new [] {new HashEntry(platform.Id, serializedPlat)});
        }

        public IEnumerable<Platform?> GetAllPlatforms()
        {
            var result = new List<Platform?>();
            var db = _connectionMultiplexer.GetDatabase();
            var completeHash = db.HashGetAll(PlatformsHashName);
            if (completeHash != null && completeHash.Any())
                result.AddRange(completeHash.Select(val => JsonSerializer.Deserialize<Platform>(val.Value)));
            return result;
        }

        public Platform? GetPlatform(string id)
        {
            Platform? result = null;
            var db = _connectionMultiplexer.GetDatabase();
            var platAsString = db.HashGet(PlatformsHashName,id);
            if (!string.IsNullOrWhiteSpace(platAsString)) result = JsonSerializer.Deserialize<Platform>(platAsString); 
            return result;
        }
    }
}