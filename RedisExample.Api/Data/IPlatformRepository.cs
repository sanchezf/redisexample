using System.Collections.Generic;
using RedisExample.Api.Models;

namespace RedisExample.Api.Data
{
    public interface IPlatformRepository
    {
        void CreatePlatform(Platform platform);
        IEnumerable<Platform?> GetAllPlatforms();
        Platform? GetPlatform(string id);
    }
}