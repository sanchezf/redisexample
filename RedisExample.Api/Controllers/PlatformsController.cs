using Microsoft.AspNetCore.Mvc;
using RedisExample.Api.Data;
using RedisExample.Api.Models;

namespace RedisExample.Api.Controllers
{
    [ApiController]
    [Route(RootRoute)]
    public class PlatformsController : ControllerBase
    {
        private readonly IPlatformRepository _repository;
        private const string RootRoute = "api/[controller]";


        public PlatformsController(IPlatformRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet]
        public IActionResult GetPlatforms()
        {
            return Ok(_repository.GetAllPlatforms());
        }
        
        
        [HttpGet("{id}")]
        public IActionResult GetPlatform(string id)
        {
            var platform = _repository.GetPlatform(id);
            if (platform == null) return NotFound();
            return Ok(platform);
        }
        
        
        [HttpPost]
        public IActionResult AddPlatform(Platform platform)
        {
            _repository.CreatePlatform(platform);
            return CreatedAtAction(nameof(GetPlatform), new {id = platform.Id},platform);
        }
    }
}